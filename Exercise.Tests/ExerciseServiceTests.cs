﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using WebApi.Exercise;

namespace WebApi.Tests
{
    [TestClass]
    public class ExerciseServiceTests
    {
        [TestMethod]
        public void IdReturnsIterator()
        {

            var id = Guid.NewGuid();

            var service1 = new ExerciseService();
            var iterator1 = service1.NewGenerator(id);

            Assert.IsNotNull(iterator1);

            var service2 = new ExerciseService();
            var iterator2 = service1.Generator(id);

            Assert.AreEqual(iterator1.Id, id);
            Assert.AreEqual(iterator1, iterator2);
        }

        [TestMethod]
        public void QuestionCorrect()
        {

            // TODO : Create sperate class to handle incrementing results, then test that separarately. 
            // eg, answer question correctly 3 times, then check that Result Incrementer.IncrementScore is called

            var service1 = new ExerciseService();
            var q = new Question { Number1 = 10, Number2 = 20 };
            var score = new ExerciseScore { Rank = 0, ScoreForRound = 0, LastResultFail = true, TimeAllowedInSeconds = 10 };

            service1.MarkQuestion(q, 30, score);

            Assert.IsFalse(score.LastResultFail);
            Assert.AreEqual(score.Rank, 0);
            Assert.AreEqual(score.TimeAllowedInSeconds, 10);
            Assert.AreEqual(score.ScoreForRound, 1);

            service1.MarkQuestion(q, 30, score);
            Assert.IsFalse(score.LastResultFail);
            Assert.AreEqual(score.Rank, 0);
            Assert.AreEqual(score.TimeAllowedInSeconds, 10);
            Assert.AreEqual(score.ScoreForRound, 2);

                        service1.MarkQuestion(q, 30, score);
            Assert.IsFalse(score.LastResultFail);
            Assert.AreEqual(score.Rank, 0);
            Assert.AreEqual(score.TimeAllowedInSeconds, 10);
            Assert.AreEqual(score.ScoreForRound, 3);

            service1.MarkQuestion(q, 30, score);
            Assert.IsFalse(score.LastResultFail);
            Assert.AreEqual(score.Rank, 1);
            Assert.AreEqual(score.TimeAllowedInSeconds, 9);
            Assert.AreEqual(score.ScoreForRound, 0);

        }

        [TestMethod]
        public void IncorrectQuestionResetsResults()
        {
            var service1 = new ExerciseService ();
            var q = new Question { Number1 = 10, Number2 = 20 };
            var score = new ExerciseScore { Rank = 2, ScoreForRound = 2, LastResultFail = false, TimeAllowedInSeconds = 3 };

            service1.MarkQuestion(q, 31, score);

            Assert.IsTrue(score.LastResultFail);
            Assert.AreEqual(score.Rank, 0);
            Assert.AreEqual(score.TimeAllowedInSeconds, 10);
            Assert.AreEqual(score.ScoreForRound, 0);
        }
    }
}
