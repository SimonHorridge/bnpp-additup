using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using WebApi.Exercise;

namespace WebApi.Tests
{
    [TestClass]
    public class QuestionIteratorTests
    {
        [TestMethod]
        public void QuestionIteratorIterates()
        {
            var iterator = new QuestionGenerator(new Guid());
            var q1 = iterator.Next(100);
            Assert.AreEqual(q1, iterator.Current);


            var q2 = iterator.Next(100);
            Assert.AreEqual(q2, iterator.Current);

            Assert.AreNotEqual(q1, iterator.Current);
            Assert.AreNotEqual(q1, q2);
        }
        [TestMethod]
        public void QuestionIteratorNextNotChangeWithCall()
        {
            var iterator = new QuestionGenerator(new Guid());
            var q1 = iterator.Next(100);

            Assert.AreEqual(q1, iterator.Current);

            var q2 = iterator.Current;

            Assert.AreEqual(q1, iterator.Current);
            Assert.AreEqual(q1, q2);
        }

        //TODO - implement this test
        //[TestMethod]
        //public void NewQuestionHasRandomNumbers()
        //{
        //    //TODO, create class, NumberGenerator with method Next() which generated random number.
        //    //Create a mock ot this whiich returns deteminitsic values.
        //    // replace initialisation of numbers 1 and 2 with NumberGenerator.Next();         
        //}
    }
}
