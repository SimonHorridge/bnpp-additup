﻿using Microsoft.AspNetCore.Mvc;
using System;
using WebApi.Exercise;
using WebApi.Model;
using WebApplication2.Models;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    public class ExerciseController : Controller
    {
        const int MaxNumber = 100;
        IExerciseService _exerciseService;

        public ExerciseController(IExerciseService exerciseService)
        {
            _exerciseService = exerciseService;
        }
        // GET api/exercise
        [HttpGet]
        [Route("")]
        public ExerciseModel GetExercise()
        {
            var id = Guid.NewGuid();
            var questionGenerator = _exerciseService.NewGenerator(id);
            var question = questionGenerator.Next(MaxNumber);
            var exerciseScore = _exerciseService.NewScore(id);

            return new ExerciseModel
            {
                ExerciseScore = exerciseScore,
                Question = question,
                Id = id,
            };
        }

        // POST api/exercise
        [HttpPost]
        public ExerciseModel PostAnswer([FromBody]ExercisePostModel model)
        {
            var questionGenerator = _exerciseService.Generator(model.Id);
            var q = questionGenerator.Current;

            var score = _exerciseService.ScoreForExercise(model.Id);
             _exerciseService.MarkQuestion(q, model.Answer, score);

            return new ExerciseModel
            {
                Question = questionGenerator.Next(MaxNumber),
                ExerciseScore = score,
                Id = model.Id,
            };
        }
    }
}
