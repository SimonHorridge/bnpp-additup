﻿export class ExerciseScore {
    scoreForRound: number;
    rank: number;
    lastResultFail: boolean;
    timeAllowedInSeconds: number;
}
