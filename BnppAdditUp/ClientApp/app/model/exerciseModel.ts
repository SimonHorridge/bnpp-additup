﻿import { ExerciseScore } from "./ExerciseScore";
import { Question } from "./Question";

export class ExerciseModel {
    question: Question;
    exerciseScore: ExerciseScore;
    id: string;
}
