/// <reference path="../../../../node_modules/@types/jasmine/index.d.ts" />
import { assert } from 'chai';
import { AddItUpComponent } from './addItUp.component';
import { FormsModule } from '@angular/forms';
import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { HttpModule } from '@angular/http';


let fixture: ComponentFixture<AddItUpComponent>;

describe('addItUp component', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({ declarations: [AddItUpComponent], imports: [FormsModule, HttpModule], providers:[ { provide: 'BASE_URL', useFactory: ()=> 'local' }]});
        fixture = TestBed.createComponent(AddItUpComponent);
        fixture.detectChanges();
    });

    it('should display a title', async(() => {
        const titleText = fixture.nativeElement.querySelector('h1').textContent;
        expect(titleText).toEqual('Add-it-up');
    }));

    it('should display a start button ', async(() => {
        const titleText = fixture.nativeElement.querySelector('button').textContent;
        expect(titleText).toEqual('Click here to start a new game');
    }));

    it('game panel should not be visible ', async(() => {
        const titleText = fixture.nativeElement.querySelector('#game');
        expect(titleText).toBeNull();
    }));

});
    