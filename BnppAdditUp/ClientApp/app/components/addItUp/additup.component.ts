import { Component, Inject } from '@angular/core';
import { ExerciseModel } from '../../model/exerciseModel';
import { Http } from '@angular/http';


@Component({
    selector: 'addItUp',
    templateUrl: './addItUp.component.html'
})
export class AddItUpComponent {

    http: Http;
    baseUrl: string;
    public answer: number | "";
    public game: ExerciseModel;
    public timeRemaining: number;
    private timeout: number;

    constructor(http: Http, @Inject('BASE_URL') baseUrl: string) {
        this.http = http;
        this.baseUrl = baseUrl;

        console.log("this.game", this.game);
    }

    public newGame() {
        console.log("calling newGame");
        clearTimeout(this.timeout);
        this.http.get(
            this.baseUrl + 'api/Exercise').subscribe(result => {
                this.game = result.json() as ExerciseModel;
                console.log("this.game", this.game);
                this.startTimer(this.game.exerciseScore.timeAllowedInSeconds);
            }, error => console.error(error)
        );
    }

    public submitAnswer() {
        console.log("submitAnswering");
        clearTimeout(this.timeout);

        this.game && this.http.post(this.baseUrl + 'api/Exercise', {
            id: this.game.id,
            answer: this.answer
        }).subscribe(result => {
            this.game = result.json() as ExerciseModel;
            console.log("this.game", this.game);
            this.startTimer(this.game.exerciseScore.timeAllowedInSeconds);
        }, error => console.error(error));
    }

    startTimer(timeSeconds: number) {
        this.timeRemaining = timeSeconds;
        this.timeout = setTimeout( () => this.countDown(), 1000);
    }

    countDown() {
        if (this.timeRemaining-- <= 0) {
            this.timeRemaining = 0;
            this.submitAnswer();
        } else {
            this.timeout = setTimeout(() => this.countDown(), 1000);
        }
    }

    public rankName() {
        var rtn = ["Beginner", "Talented", "Intermediate", "Advanced", "Expert"][this.game.exerciseScore.rank];
        return rtn || "Level " + this.game.exerciseScore.rank;
    }
}
