﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Exercise
{
    public class ExerciseService : IExerciseService
    {
        static Dictionary<Guid, IQuestionGenerator> _iterators = new Dictionary<Guid, IQuestionGenerator>();
        static Dictionary<Guid, ExerciseScore> _scores = new Dictionary<Guid, ExerciseScore>();

        public int InitialTimeInSeconds { get { return 10; } }  //Todo - pass from config
        public int ScoreForPromotion { get { return 3; } } //Todo - pass from config

        public IQuestionGenerator Generator(Guid id)
        {
            if (_iterators.ContainsKey(id))
            {
                return _iterators[id];
            }
            return null;
        }

        public ExerciseScore ScoreForExercise(Guid id)
        {
            return _scores[id];
        }

        public void MarkQuestion(Question question, int answer, ExerciseScore exerciseScore)
        {
            if (answer == question.Number1 + question.Number2)
            {
                IncrementScore(exerciseScore);
            }
            else
            {
                ResetScore(exerciseScore);
            }
        }

        private void ResetScore(ExerciseScore score)
        {
            score.LastResultFail = true;
            score.Rank = 0;
            score.ScoreForRound = 0;
            score.TimeAllowedInSeconds = InitialTimeInSeconds;
        }

        private void IncrementScore(ExerciseScore score)
        {
            score.LastResultFail = false;
            if (score.ScoreForRound++ >= ScoreForPromotion)
            {
                score.Rank = score.Rank + 1;
                score.TimeAllowedInSeconds = score.TimeAllowedInSeconds - 1;
                score.ScoreForRound = 0;
            }
        }
        public ExerciseScore NewScore(Guid id)
        {
            var rtn = new ExerciseScore();
            ResetScore(rtn);
            rtn.LastResultFail = false;
            _scores.Add(id, rtn);
            return rtn;
        }

        public IQuestionGenerator NewGenerator(Guid id)
        {
            var rtn = new QuestionGenerator(id);
            _iterators.Add(id, rtn);
            return rtn;
        }
    }
}
