﻿namespace WebApi.Exercise
{
    public class ExerciseScore
    {
        public int ScoreForRound { get; set; }
        public int Rank { get; set; }
        public bool LastResultFail { get; set; }
        public int TimeAllowedInSeconds { get; set; }
    }
}
