﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Exercise
{
    public class Question
    {
        public int Number1 { get; set; }
        public int Number2 { get; set; }
    }
}
