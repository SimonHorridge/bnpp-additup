﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Exercise
{
    public class QuestionGenerator : IQuestionGenerator
    {
        readonly private Guid _id;

        public QuestionGenerator(Guid id)
        {
            _id = id;
        }
        public Question Current { get; private set; }
        public Guid Id { get => _id;}

        public Question Next(int max)
        {
            return Current = new Question
            {
                Number1 = new Random().Next(1, max),
                Number2 = new Random().Next(1, max),
            };
        }
    }
}
