﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Exercise
{
    public interface IExerciseService
    {
        int InitialTimeInSeconds { get; }
        int ScoreForPromotion { get; }

        IQuestionGenerator NewGenerator(Guid id);
        IQuestionGenerator Generator(Guid id);

        ExerciseScore NewScore(Guid id);
        void MarkQuestion(Question question, int answer, ExerciseScore score);
        ExerciseScore ScoreForExercise(Guid id);
    }
}
