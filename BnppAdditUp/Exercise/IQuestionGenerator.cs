﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Exercise
{
    public interface IQuestionGenerator
    {
        Question Current { get; }
        Guid Id { get; }

        Question Next(int max);
    }
}
