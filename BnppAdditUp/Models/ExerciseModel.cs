﻿using System;
using WebApi.Exercise;

namespace WebApi.Model
{
    public class ExerciseModel
    {
        public Question Question { get; set; }
        public ExerciseScore ExerciseScore { get; set; }
        public Guid Id { get; set; }
    }
}
