﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication2.Models
{
    public class ExercisePostModel
    {
        public Guid Id { get; set; }
        public int Answer { get; set; }
    }
}
